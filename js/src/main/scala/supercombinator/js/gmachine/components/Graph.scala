package supercombinator.js.gmachine.components

import monix.reactive.Observable
import outwatch.dom._
import outwatch.dom.dsl.{id, _}
import outwatch.dom.VDomModifier
import supercombinator.js.facades.sigma.{Edge, GraphData, Node, Sigma}
import supercombinator.js.facades.dagre.{
  Dagre,
  EdgeMutationFn,
  Graph => dargeGraph,
  GraphProps,
  NodeProps,
}
import monix.execution.Scheduler.Implicits.global
import supercombinator.gmachine.machine.{Diff, DiffWithHash}
import supercombinator.js.models.VisualizationState

final case class Graph(
  graphNode: Handler[VisualizationState] => Observable[VDomModifier],
) {
  def node(
    propsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] = graphNode(propsHandler)
}

object Graph {
  def createNode(
    propsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] =
    for {
      _ <- propsHandler
    } yield {

      def displayAttrByStep: Int => String = {
        case -1 => "none"
        case _  => "block"
      }

      def drawGraph(diffs: List[DiffWithHash], currentStep: Int): Unit = {
        val sigma = new Sigma("graph");

        val dgr = new dargeGraph()
          .setGraph(GraphProps.default())
          .setDefaultEdgeLabel(EdgeMutationFn.default())

        dgr.graph().ranker = "tight-tree"
        println(diffs)
        diffs
          .slice(0, currentStep + 1).foreach { diff =>
            val add = diff.add
            val remove = diff.remove
            val addNodes = add.nodes
            val removeNodes = remove.nodes
            removeNodes.foreach { node =>
              val needsToUpdate =
                removeNodes.length == 1 && addNodes.length == 1 && removeNodes.head.id == addNodes.head.id
              if (needsToUpdate) dgr.removeNode(node.id)
              if (node.from.isDefined && node.to.isDefined) {
                dgr.removeEdge(node.id, node.to.get)
                dgr.removeEdge(node.id, node.from.get)
              }
            }
            addNodes.foreach { node =>
              dgr.setNode(
                node.id,
                NodeProps.labeledProps(s"${node.label}($$${node.id})"),
              )
              if (node.from.isDefined && node.to.isDefined) {
                dgr.setEdge(node.id, node.from.get)
                dgr.setEdge(node.id, node.to.get)
              }
            }
          }
        Dagre.layout(dgr)

        val nodesList: List[Node] = dargeGraph
          .arrayToList(dgr.nodes()).map { nodeId =>
            val node = dgr.node(nodeId)
            Node(
              id = nodeId,
              label = node.label,
              x = node.x.toInt,
              y = node.y.toInt,
              size = 1,
              color = "#92ee1c",
            )
          }
        val edgeList: List[Edge] = dargeGraph
          .arrayToList(dgr.edges()).map { edge =>
            Edge(
              id = s"${edge.v}->${edge.w}",
              source = edge.v,
              target = edge.w,
            )
          }

        sigma.graph
          .clear().read(
            GraphData(
              edges = edgeList,
              nodes = nodesList,
            ),
          )

        sigma.refresh()
      }

      div(
        cls := "graph__wrapper",
        styleAttr <-- propsHandler.map {
          case VisualizationState(_, currentStep, _) =>
            s"display: ${displayAttrByStep(currentStep)}"
        },
        div(
          className := "graph",
          id := "graph",
          onDomMount.foreach { element =>
            element.innerHTML = ""
            propsHandler.map { case VisualizationState(diffs, currentStep, _) =>
              drawGraph(diffs, currentStep)
            }.runAsyncGetFirst
          },
        ),
      )
    }

  def init(): Observable[Graph] = Observable {
    Graph(propsHandler => createNode(propsHandler))
  }
}
