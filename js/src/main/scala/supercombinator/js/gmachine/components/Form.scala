package supercombinator.js.gmachine.components

import cats.effect.{ContextShift, IO}
import cats.implicits.catsSyntaxOptionId
import monix.execution.Scheduler.Implicits.global
import monix.reactive.Observable
import org.scalajs.dom.console
import org.scalajs.dom.window._
import outwatch.dom._
import outwatch.dom.dsl._
import supercombinator.gmachine.machine.{Diff, DiffResponse}
import supercombinator.js.http.Client
import supercombinator.models.{InputGCode, InputLambda, Output}
import supercombinator.js.models.VisualizationState

import scala.concurrent.duration._

final case class Form(
  formNode: (
    Handler[Boolean],
    Handler[VisualizationState],
    Handler[String],
    Handler[String],
  ) => Observable[VDomModifier],
) {
  def node(
    isModalHiddenHandler: Handler[Boolean],
    visualizationPropsHandler: Handler[VisualizationState],
    codeTypeHandler: Handler[String],
    codeHandler: Handler[String],
  ): Observable[VDomModifier] =
    formNode(
      isModalHiddenHandler,
      visualizationPropsHandler,
      codeTypeHandler,
      codeHandler,
    )
}

object Form {
  val DEFAULT_CODE = "BEGIN\nPUSHINT 2\nPUSHINT 2\nADD\nEND"
  val DEFAULT_LAMBDA_CODE = "letrec sqr = \\x -> ((* x) x) in  (sqr 100)"

  private def requestsGCode(
    urls: Observable[String],
    code: String,
    withErrors: Boolean,
  )(implicit
    contextShift: ContextShift[IO],
  ): Observable[DiffResponse] =
    urls
      .flatMap(url =>
        Observable.from(
          new Client(url).requestGCode(
            InputGCode(code, onlyResult = false, withErrors),
          ),
        ),
      ).map(maybeResp =>
        maybeResp
          .getOrElse(DiffResponse(List(), "Incorrect JSON".some, None)),
      )

  def timeoutWait(): Observable[Unit] =
    Observable.unit.delayExecution(0.seconds)

  private def requestsLambdaCode(
    urls: Observable[String],
    code: String,
  )(implicit
    contextShift: ContextShift[IO],
  ): Observable[Output] =
    urls
      .flatMap(url =>
        Observable.from(
          new Client(url).requestLambda(InputLambda(code)),
        ),
      ).map(maybeResp => maybeResp.getOrElse(Output(List())))

  def createNode(
    isModalHiddenHandler: Handler[Boolean],
    visualizationPropsHandler: Handler[VisualizationState],
    codeTypeHandler: Handler[String],
    codeHandler: Handler[String],
  )(implicit
    contextShift: ContextShift[IO],
  ): Observable[VDomModifier] = {
    Observable {
      val url = Observable("http://localhost:8080")

      val isIncreaseButtonDisabled = for {
        visualizationProps <- visualizationPropsHandler
      } yield visualizationProps.isLast

      val isDecreaseButtonDisabled = for {
        visualizationProps <- visualizationPropsHandler
      } yield visualizationProps.step <= 0

      div(
        cls := "form",
        form(
          cls := "form__container",
          textArea(
            cls := "form__textarea",
            value <-- codeHandler,
            onChange.value --> codeHandler,
          ),
          div(
            cls := "form__buttons",
            button(
              "Отправить",
              cls := "form__button form__button--send",
              tpe := "button",
              onClick.foreach {
                val execution =
                  for {
                    _                  <- timeoutWait()
                    codeType           <- codeTypeHandler
                    code               <- codeHandler
                    visualizationProps <- visualizationPropsHandler
                    response <- codeType match {
                      case "gcode" =>
                        requestsGCode(url, code, visualizationProps.withErrors)
                      case "lambda" => requestsLambdaCode(url, code)
                    }
                    visualizationProps <- visualizationPropsHandler
                  } yield
                    response match {
                      case gResult: DiffResponse =>
                        gResult.result match {
                          case Some(result) =>
                            alert(s"Result: $result")
                            val withErrors = visualizationProps.withErrors
                            codeHandler.onNext(code)
                            visualizationPropsHandler.onNext(
                              VisualizationState(gResult.diff, 0, withErrors),
                            )
                          case _ => alert("Error!")
                        }
                      case lambdaResult: Output =>
                        val lambdaCode = response
                          .asInstanceOf[Output].result.reduce((a, b) =>
                            s"$a\n$b",
                          )
                        codeHandler.onNext(lambdaCode)
                    }

                execution
                  .map(_ => codeTypeHandler.onNext("gcode")).runAsyncGetFirst

                ()
              },
            ),
            button(
              for {
                visualizationProps <- visualizationPropsHandler
              } yield
                if (visualizationProps.withErrors)
                  "С ошибками"
                else
                  "Без ошибок",
              cls := "form__button form__button--with-errors",
              tpe := "button",
              onClick(for {
                visualizationProps <- visualizationPropsHandler
              } yield {
                val newWithErrorsState = !visualizationProps.withErrors
                visualizationProps.copy(withErrors = newWithErrorsState)
              }) --> visualizationPropsHandler,
            ),
            button(
              for {
                codeType <- codeTypeHandler
              } yield
                codeType match {
                  case "gcode"  => "G"
                  case "lambda" => "λ"
                },
              cls := "form__button form__button--change-code-type",
              tpe := "button",
              onClick(
                for {
                  codeType <- codeTypeHandler
                } yield
                  codeType match {
                    case "gcode"  => "lambda"
                    case "lambda" => "gcode"
                  },
              ) --> codeTypeHandler,
              onClick(
                for {
                  codeType <- codeTypeHandler
                } yield
                  codeType match {
                    case "gcode"  => DEFAULT_CODE
                    case "lambda" => DEFAULT_LAMBDA_CODE
                  },
              ) --> codeHandler,
            ),
            button(
              "Справка",
              tpe := "button",
              cls := "form__button form__button--modal",
              onClick(isModalHiddenHandler.map(!_)) --> isModalHiddenHandler,
            ),
            button(
              "+",
              cls := "form__button form__button--more",
              tpe := "button",
              disabled <-- isIncreaseButtonDisabled,
              onClick(
                for {
                  visualizationProps <- visualizationPropsHandler
                } yield visualizationProps.nextStep,
              ) --> visualizationPropsHandler,
            ),
            button(
              "-",
              cls := "form__button form__button--less",
              tpe := "button",
              disabled <-- isDecreaseButtonDisabled,
              onClick(
                for {
                  visualizationProps <- visualizationPropsHandler
                } yield visualizationProps.prevStep,
              ) --> visualizationPropsHandler,
            ),
          ),
        ),
      )
    }
  }

  def init()(implicit
    contextShift: ContextShift[IO],
  ): IO[Form] = IO {
    Form(
      (
        isModalHiddenHandler,
        visualizationPropsHandler,
        codeTypeHandler,
        codeHandler,
      ) =>
        createNode(
          isModalHiddenHandler,
          visualizationPropsHandler,
          codeTypeHandler,
          codeHandler,
        ),
    )
  }
}
