package supercombinator.js.gmachine.components

import cats.effect.IO
import monix.reactive.Observable
import monix.execution.Scheduler.Implicits.global
import outwatch.dom._
import outwatch.dom.dsl._
import supercombinator.js.models.VisualizationState
import supercombinator.js.Utils._
import org.scalajs.dom.window.alert
import supercombinator.js.facades.md5.MD5
import supercombinator.gmachine.machine.Utils.hashIdx

final case class Visualization(
  visualizationNode: Handler[VisualizationState] => Observable[VDomModifier],
) {
  def node(
    visualizationPropsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] = visualizationNode(visualizationPropsHandler)
}

object Visualization {
  def createNode(
    visualizationPropsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] =
    for {
      graphNode          <- Graph.createNode(visualizationPropsHandler)
      stackNode          <- Stack.createNode(visualizationPropsHandler)
      visualizationProps <- visualizationPropsHandler
      erroneousCommandIndexHandler <- ioToObservable(Handler.create[Int](-1))
    } yield {
      val diffs = visualizationProps.diffs
      val currentStep = visualizationProps.step
      val withErrors = visualizationProps.withErrors

      def commandClass: (Int, Int) => String = (tabIndex, currentStep) =>
        if (tabIndex == currentStep) {
          "visualization__command visualization__command--active"
        } else {
          "visualization__command"
        }

      div(
        cls := "visualization",
        graphNode,
        stackNode,
        if (currentStep == -1) {
          ""
        } else {
          div(
            cls := "visualization__commands-container",
            div(
              cls := "visualization__commands",
              styleAttr := s"bottom: ${currentStep * 27}px",
              diffs.zipWithIndex.map { case (diffElement, index) =>
                div(
                  cls := commandClass(index, currentStep),
                  if (withErrors) {
                    input(
                      tpe := "radio",
                      name := "erroneous-command",
                      id := s"command-checkbox-$index",
                      cls := "visualization__command-checkbox",
                      value := s"$index",
                      onChange(index) --> erroneousCommandIndexHandler,
                    )
                  } else {
                    ""
                  },
                  label(
                    diffElement.command,
                    cls := "visualization__command-label",
                    `for` := s"command-checkbox-$index",
                  ),
                )
              },
            ),
            if (withErrors) {
              div(
                cls := "visualization__buttons-wrapper",
                button(
                  "Выбрать ошибочную строку",
                  cls := "visualization__button",
                  onClick.foreach {
                    val execution = for {
                      erroneousCommandIndex <- erroneousCommandIndexHandler
                    } yield {
                      val hash =
                        hashIdx(
                          erroneousCommandIndex,
                          hasError = true,
                          MD5.apply,
                        )
                      if (
                        erroneousCommandIndex < diffs.length && diffs(
                          erroneousCommandIndex,
                        ).hash == hash
                      ) {
                        alert("Верно")
                      } else {
                        alert("Не верно")
                      }
                    }

                    execution.runAsyncGetFirst

                    ()
                  },
                ),
                button(
                  "Нет ошибочной строки",
                  cls := "visualization__button",
                  onClick.foreach {
                    val noDiffsWithError =
                      diffs.zipWithIndex.foldRight(true) { (diffObj, correct) =>
                        val (diff, index) = diffObj
                        val hash = hashIdx(index, hasError = true, MD5.apply)
                        correct && (hash != diff.hash)
                      }

                    if (noDiffsWithError) {
                      alert("Верно")
                    } else {
                      alert("Не верно")
                    }
                    ()
                  },
                ),
              )
            } else {
              ""
            },
          )
        },
      )
    }

  def init(): IO[Visualization] = IO {
    Visualization(visualizationPropsHandler =>
      createNode(visualizationPropsHandler),
    )
  }
}
