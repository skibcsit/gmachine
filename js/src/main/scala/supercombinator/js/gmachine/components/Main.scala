package supercombinator.js.gmachine.components

import cats.effect.{ContextShift, IO}
import monix.reactive.Observable
import outwatch.dom._
import outwatch.dom.dsl._
import supercombinator.js.gmachine.components.Form.DEFAULT_CODE
import supercombinator.js.models.VisualizationState

final case class Main(
  visualization: Observable[VDomModifier],
  form: VDomModifier,
  infoModal: Observable[VDomModifier],
) {

  def node(): Observable[VDomModifier] =
    for {
      infoModalNode     <- infoModal
      visualizationNode <- visualization
    } yield
      VDomModifier(
        div(
          cls := "main",
          visualizationNode,
          form,
          infoModalNode,
        ),
      )
}

object Main {
  def init()(implicit
    contextShift: ContextShift[IO],
  ): IO[Main] =
    for {
      withErrorsHandler    <- Handler.create[Boolean](false)
      isModalHiddenHandler <- Handler.create[Boolean](true)
      infoModal            <- InfoModal.init()
      visualizationPropsHandler <- Handler.create[VisualizationState](
        VisualizationState(List(), -1, withErrors = false),
      )
      codeTypeHandler <- Handler.create[String]("gcode")
      codeHandler     <- Handler.create[String](DEFAULT_CODE)
      form            <- Form.init()
    } yield
      Main(
        Visualization.createNode(visualizationPropsHandler),
        form.node(
          isModalHiddenHandler,
          visualizationPropsHandler,
          codeTypeHandler,
          codeHandler,
        ),
        infoModal.node(isModalHiddenHandler),
      )
}
