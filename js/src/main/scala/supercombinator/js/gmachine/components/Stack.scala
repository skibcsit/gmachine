package supercombinator.js.gmachine.components

import monix.reactive.Observable
import outwatch.dom._
import outwatch.dom.dsl.{id, _}
import outwatch.dom.VDomModifier
import supercombinator.js.facades.sigma.{Edge, GraphData, Node, Sigma}
import supercombinator.js.facades.dagre.{
  Dagre,
  EdgeMutationFn,
  Graph => dargeGraph,
  GraphProps,
  NodeProps,
}
import monix.execution.Scheduler.Implicits.global
import supercombinator.gmachine.machine.{Diff, DiffWithHash}
import supercombinator.js.models.VisualizationState

final case class Stack(
  graphNode: Handler[VisualizationState] => Observable[VDomModifier],
) {
  def node(
    visualizationPropsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] = graphNode(visualizationPropsHandler)
}

object Stack {
  def createNode(
    propsHandler: Handler[VisualizationState],
  ): Observable[VDomModifier] =
    for {
      _ <- propsHandler
    } yield {
      def displayAttrByStep: Int => String = {
        case -1 => "none"
        case _  => "block"
      }

      def drawGraph(diffs: List[DiffWithHash], currentStep: Int): Unit = {
        val sigma = new Sigma("stack");

        val dgr = new dargeGraph()
          .setGraph(GraphProps.default())
          .setDefaultEdgeLabel(EdgeMutationFn.default())

        dgr.graph().rankDir = "LR"
        for ((node, index) <- diffs(currentStep).stack.view.zipWithIndex) {
          dgr.setNode(index, NodeProps.labeledProps(s"$$${node.id}"))
          if (index > 0) dgr.setEdge(index - 1, index)
        }
        Dagre.layout(dgr)

        val nodesList: List[Node] = dargeGraph
          .arrayToList(dgr.nodes()).map { nodeId =>
            val node = dgr.node(nodeId)
            Node(
              id = nodeId,
              label = node.label,
              x = node.x.toInt,
              y = node.y.toInt,
              size = 1,
              color = "#92ee1c",
            )
          }
        val edgeList: List[Edge] = dargeGraph
          .arrayToList(dgr.edges()).map { edge =>
            Edge(
              id = s"${edge.v}->${edge.w}",
              source = edge.v,
              target = edge.w,
            )
          }

        sigma.graph
          .clear().read(
            GraphData(
              edges = edgeList,
              nodes = nodesList,
            ),
          )

        sigma.refresh()
      }

      div(
        cls := "stack__wrapper",
        styleAttr <-- propsHandler.map {
          case VisualizationState(_, currentStep, _) =>
            s"display: ${displayAttrByStep(currentStep)}"
        },
        div(
          className := "stack",
          id := "stack",
          onDomMount.foreach { element =>
            element.innerHTML = ""
            propsHandler.map { case VisualizationState(diffs, currentStep, _) =>
              drawGraph(diffs, currentStep)
            }.runAsyncGetFirst
          },
        ),
      )
    }

  def init(): Observable[Stack] = Observable {
    Stack(propsHandler => createNode(propsHandler))
  }
}
