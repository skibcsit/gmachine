package supercombinator.js.gmachine.components
import cats.effect.{ExitCode, IO, IOApp}
import monix.execution.Scheduler.Implicits.global
import outwatch.dom._
import outwatch.dom.dsl._

object GMachine extends IOApp {
  def run(args: List[String]): IO[ExitCode] =
    for {
      main <- Main.init
      _ <- OutWatch.renderReplace(
        "#root",
        div(
          cls := "gmachine-app",
          ModifierStreamReceiver(ValueObservable(main.node())),
        ),
      )
    } yield ExitCode.Success
}
