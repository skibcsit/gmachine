package supercombinator.js

import cats.effect.{ContextShift, IO}
import monix.catnap.SchedulerEffect
import monix.execution.Scheduler
import monix.reactive.Observable

object Utils {
  def observableToIO[T](
    observable: Observable[T],
  )(implicit s: Scheduler): IO[T] = {
    implicit val cs: ContextShift[IO] =
      SchedulerEffect.contextShift[IO](s)(IO.ioEffect)
    IO.fromFuture(IO(observable.runAsyncGetFirst(s).map(_.get)(s)))(cs)
  }

  def ioToObservable[T](
    io: IO[T],
  ): Observable[T] =
    Observable.fromFuture(io.unsafeToFuture())
}
