package supercombinator.js.http

import cats.effect.{ContextShift, IO}
import supercombinator.gmachine.machine.DiffResponse
import supercombinator.models
import supercombinator.models.{InputGCode, InputLambda, Output}

class Client(
  protocolHost: String,
) {

  def requestGCode(
    data: InputGCode,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[Either[models.Error, DiffResponse]] =
    IO.fromFuture(IO(Api(protocolHost).gCodeCompile(data).toFuture))

  def requestLambda(
    data: InputLambda,
  )(implicit
    contextShift: ContextShift[IO],
  ): IO[Either[models.Error, Output]] =
    IO.fromFuture(IO(Api(protocolHost).lambdaToGMachineCompile(data).toFuture))

}
