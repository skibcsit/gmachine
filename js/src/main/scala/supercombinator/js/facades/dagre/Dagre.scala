package supercombinator.js.facades.dagre

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait GraphProps extends js.Object {
  /* now empty, but can container some props */
}

object GraphProps {
  def default(): GraphProps = new js.Object().asInstanceOf[GraphProps]
}

@js.native
trait EdgeMutationFn extends js.Function {
  /* now empty, but can container some props */
}

object EdgeMutationFn {
  def default(): EdgeMutationFn = { () =>
    new js.Object()
  }.asInstanceOf[EdgeMutationFn]
}

@js.native
trait NodeProps extends js.Object {
  val label: String
}

object NodeProps {
  def labeledProps(label: String): NodeProps =
    js.Dictionary("label" -> label).asInstanceOf[NodeProps]
}

@js.native
trait GraphState extends js.Object {
  var rankDir: String
  var ranker: String
}

@js.native
trait GraphNode extends js.Object {
  val x: Double = js.native
  val y: Double = js.native
  val label: String = js.native
}

@js.native
trait GraphEdge extends js.Object {
  val v: String = js.native
  val w: String = js.native
}

@JSGlobal("dagre.graphlib.Graph")
@js.native
class Graph() extends js.Object {
  def setGraph(props: GraphProps): Graph = js.native
  def setDefaultEdgeLabel(mutationFn: EdgeMutationFn): Graph = js.native

  def graph(): GraphState = js.native
  def setNode(index: Int, props: NodeProps): Unit = js.native
  def setEdge(from: Int, to: Int): Unit = js.native
  def removeNode(index: Int): Unit = js.native
  def removeEdge(from: Int, to: Int): Unit = js.native

  def nodes(): js.Array[String] = js.native
  def edges(): js.Array[GraphEdge] = js.native

  def node(id: String): GraphNode = js.native
}

object Graph {
  def arrayToList[T](array: js.Array[T]): List[T] =
    array.toList
}

@js.native
trait Graphlib extends js.Object {
  val graph: Graph
}

@js.native
@JSGlobal("dagre")
object Dagre extends js.Object {
  val graphlib: Graphlib = js.native
  def layout(source: Graph): Unit = js.native
}
