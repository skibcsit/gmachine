package supercombinator.js.facades.sigma

import scala.scalajs.js.JSConverters._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
trait Node extends js.Object {
  val id: String
  val label: String
  val x: Int
  val y: Int
  val size: Int
  val color: String
}

object Node {
  def apply(
    id: String,
    label: String,
    x: Int,
    y: Int,
    size: Int,
    color: String,
  ): Node =
    js.Dynamic
      .literal(
        id = id,
        label = label,
        x = x,
        y = y,
        size = size,
        color = color,
      ).asInstanceOf[Node]
}

@js.native
trait Edge extends js.Object {
  val id: String
  val source: String
  val target: String
}

object Edge {
  def apply(id: String, source: String, target: String): Edge =
    js.Dynamic
      .literal(
        id = id,
        source = source,
        target = target,
      ).asInstanceOf[Edge]
}

@js.native
trait Graph extends js.Object {
  def addNode(props: Node): Graph = js.native
  def addEdge(props: Edge): Graph = js.native
  def clear(): Graph = js.native
  def read(graph: Graph): Unit = js.native
}

@js.native
trait GraphData extends js.Object {
  val edges: List[Edge]
  val nodes: List[Node]
}

object GraphData {
  def apply(edges: List[Edge], nodes: List[Node]): Graph =
    js.Dynamic
      .literal(
        edges = edges.toJSArray,
        nodes = nodes.toJSArray,
      ).asInstanceOf[Graph]
}

@js.native
@JSGlobal("sigma")
class Sigma(element: String) extends js.Object {
  val graph: Graph = js.native
  def refresh(): Unit = js.native
}
