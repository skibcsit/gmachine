package supercombinator.js.models

import supercombinator.gmachine.machine.DiffWithHash
import io.circe.generic.JsonCodec

@JsonCodec
case class VisualizationState(
  diffs: List[DiffWithHash],
  step: Int,
  withErrors: Boolean,
) {
  def isLast: Boolean = step == diffs.length - 1 || diffs.isEmpty

  def nextStep: VisualizationState =
    VisualizationState(diffs, step + 1, withErrors)

  def prevStep: VisualizationState = {
    val previousStep = step match {
      case -1 => -1
      case _  => step - 1
    }
    VisualizationState(diffs, previousStep, withErrors)
  }
}
