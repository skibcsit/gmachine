package JSSupport

import org.scalajs.dom._
import utest._

import scala.concurrent.{ExecutionContext, Future}

abstract class JSDomTestSuite extends TestSuite {
  override def utestWrap(path: Seq[String], runBody: => Future[Any])(implicit
    ec: ExecutionContext,
  ): Future[Any] = {
    document.body.innerHTML = ""

    // prepare body with <div id="root"></div>
    val root = document.createElement("div")
    root.id = "root"
    document.body.appendChild(root)

    runBody
  }
}
