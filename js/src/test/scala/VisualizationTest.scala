import JSSupport.JSDomTestSuite
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.document
import outwatch.dom.dsl.{cls, div}
import outwatch.dom.{Handler, ModifierStreamReceiver, OutWatch, ValueObservable}
import supercombinator.gmachine.machine.Diff
import supercombinator.js.gmachine.components.Visualization
import supercombinator.js.models.VisualizationState
import utest._

object VisualizationTest extends JSDomTestSuite {
  val tests: Tests = Tests {
    test("Visualization is rendered correctly") {
      for {
        visualizationPropsHandler <- Handler.create[VisualizationState](
          VisualizationState(List(), -1, withErrors = false),
        )
        node = Visualization.createNode(visualizationPropsHandler)
        _ = OutWatch
          .renderReplace(
            "#root",
            div(
              cls := "gmachine-app",
              ModifierStreamReceiver(ValueObservable(node)),
            ),
          ).unsafeRunSync()
      } yield
        document.body.firstElementChild.innerHTML ==>
          "<div class=\"visualization\">" +
          "Visualization" +
          "<div class=\"visualization__graph\"></div>" +
          "<div class=\"visualization__commands-container\">" +
          "<div class=\"visualization__commands\"></div>" +
          "</div>" +
          "</div>"
    }
  }
}
