import IOSupport.IOJSDomTestSuite
import cats.effect._
import monix.execution.Scheduler.Implicits.global
import org.scalajs.dom.document
import org.scalajs.dom.raw.HTMLTextAreaElement
import outwatch.dom.dsl._
import outwatch.dom.{EffectModifier, Handler, OutWatch}
import supercombinator.gmachine.machine.Diff
import supercombinator.js.gmachine.components.Form
import supercombinator.js.models.VisualizationState
import supercombinator.js.gmachine.components.Form.DEFAULT_CODE
import utest._

import scala.concurrent.duration._

object FormTest extends IOJSDomTestSuite {
  val tests: Tests = Tests {
    test("Form is rendered correctly") {
      for {
        isModalHiddenHandler <- Handler.create[Boolean](true)
        visualizationPropsHandler <- Handler
          .create[VisualizationState](
            VisualizationState(List(), -1, withErrors = false),
          )
        codeTypeHandler <- Handler.create[String]("gcode")
        codeHandler     <- Handler.create[String](DEFAULT_CODE)
      } yield {
        OutWatch
          .renderReplace(
            "#root",
            div(
              cls := "gmachine-app",
              Form.createNode(
                isModalHiddenHandler,
                visualizationPropsHandler,
                codeTypeHandler,
                codeHandler,
              ),
            ),
          ).unsafeRunSync()
        document.body.firstElementChild.innerHTML ==>
          "<div class=\"form\">" +
          "<form class=\"form__container\">" +
          "<textarea class=\"form__textarea\">" +
          "</textarea>" +
          "<div class=\"form__buttons\">" +
          "<button class=\"form__button form__button--send\" type=\"button\">" +
          "Отправить" +
          "</button>" +
          "<button class=\"form__button form__button--with-errors\" type=\"button\" disabled=\"\">" +
          "С ошибками" +
          "</button>" +
          "<button class=\"form__button form__button--change-code-type\" type=\"button\">" +
          "</button>" +
          "<button type=\"button\" class=\"form__button form__button--modal\">" +
          "Справка" +
          "</button>" +
          "<button class=\"form__button form__button--more\" type=\"button\" disabled=\"\">" +
          "+" +
          "</button>" +
          "<button class=\"form__button form__button--less\" type=\"button\" disabled=\"\">" +
          "-" +
          "</button>" +
          "</div>" +
          "</form>" +
          "</div>"
      }
    }

    test("Form has correct buttons") {
      for {
        isModalHiddenHandler <- Handler.create[Boolean](true)
        visualizationPropsHandler <- Handler
          .create[VisualizationState](
            VisualizationState(List(), -1, withErrors = false),
          )
        codeTypeHandler <- Handler.create[String]("gcode")
        codeHandler     <- Handler.create[String](DEFAULT_CODE)
      } yield {
        OutWatch
          .renderReplace(
            "#root",
            div(
              cls := "gmachine-app",
              Form.createNode(
                isModalHiddenHandler,
                visualizationPropsHandler,
                codeTypeHandler,
                codeHandler,
              ),
            ),
          ).unsafeRunSync()
        assert(
          document.body
            .querySelector(".form__button--with-errors").tagName == "BUTTON",
          document.body
            .querySelector(
              ".form__button--change-code-type",
            ).tagName == "BUTTON",
          document.body
            .querySelector(".form__button--modal").tagName == "BUTTON",
        )
      }
    }

    test("Form textarea has default G code after rendering") {
      for {
        isModalHiddenHandler <- Handler.create[Boolean](true)
        visualizationPropsHandler <- Handler
          .create[VisualizationState](
            VisualizationState(List(), -1, withErrors = false),
          )
        codeTypeHandler <- Handler.create[String]("gcode")
        codeHandler     <- Handler.create[String](DEFAULT_CODE)
        _ = OutWatch
          .renderReplace(
            "#root",
            div(
              cls := "gmachine-app",
              Form.createNode(
                isModalHiddenHandler,
                visualizationPropsHandler,
                codeTypeHandler,
                codeHandler,
              ),
            ),
          ).unsafeRunSync()
        _ <- Timer[IO].sleep(100.millisecond)
      } yield
        document.body
          .querySelector(".form__textarea").asInstanceOf[
            HTMLTextAreaElement,
          ].value ==>
          "BEGIN\nPUSHINT 2\nPUSHINT 2\nMUL\nEND"
    }
  }
}
