package supercombinator.models

import io.circe.generic.JsonCodec
@JsonCodec
case class Output(
  result: List[String],
)

@JsonCodec
case class InputLambda(
  code: String,
)

@JsonCodec
case class InputGCode(
  code: String,
  onlyResult: Boolean,
  withErrors: Boolean,
)

@JsonCodec
case class Error(
  err: String,
)
