package supercombinator

import endpoints4s.algebra
import endpoints4s.algebra.Endpoints
import endpoints4s.generic.JsonSchemas
import supercombinator.gmachine.machine.{
  Diff,
  DiffResponse,
  DiffWithHash,
  EdgeView,
  GraphDiff,
  NodeView,
}
import supercombinator.models.{InputGCode, InputLambda, Output}

trait GMachineApiEndpoints
    extends Endpoints
    with algebra.JsonEntitiesFromSchemas
    with JsonSchemas {
//  val lambdaToGMachineCompile: Endpoint[InputLambda, models.Error, Output, Nothing] =
//    endpoint.post
//      .in("lambda")
//      .in(jsonBody[InputLambda])
//      .errorOut(jsonBody[models.Error])
//      .out(jsonBody[Output])
//  val gCodeCompile: Endpoint[InputGCode, models.Error, DiffWithErr, Nothing] =
//    endpoint.post
//      .in("gcode")
//      .in(jsonBody[InputGCode])
//      .errorOut(jsonBody[models.Error])
//      .out(jsonBody[DiffWithErr])
  val gCodeCompile: Endpoint[InputGCode, Either[models.Error, DiffResponse]] =
    endpoint(
      post(path / "gcode", jsonRequest[InputGCode]),
      response(BadRequest, jsonResponse[models.Error])
        .orElse(ok(jsonResponse[DiffResponse])),
    )
  val lambdaToGMachineCompile
    : Endpoint[InputLambda, Either[models.Error, Output]] =
    endpoint(
      post(path / "lambda", jsonRequest[InputLambda]),
      response(BadRequest, jsonResponse[models.Error])
        .orElse(ok(jsonResponse[Output])),
    )

  implicit lazy val inputLambdaSchema: JsonSchema[InputLambda] =
    genericJsonSchema[InputLambda]
  implicit lazy val errorSchema: JsonSchema[models.Error] =
    genericJsonSchema[models.Error]
  implicit lazy val outputSchema: JsonSchema[Output] = genericJsonSchema[Output]
  implicit lazy val inputGCodeSchema: JsonSchema[InputGCode] =
    genericJsonSchema[InputGCode]
  implicit lazy val diffWithErrSchema: JsonSchema[DiffResponse] =
    genericJsonSchema[DiffResponse]
  implicit lazy val diffSchema: JsonSchema[Diff] =
    genericJsonSchema[Diff]
  implicit lazy val diffWithIdxSchema: JsonSchema[DiffWithHash] =
    genericJsonSchema[DiffWithHash]
  implicit lazy val graphDiffSchema: JsonSchema[GraphDiff] =
    genericJsonSchema[GraphDiff]
  implicit lazy val nodeViewSchema: JsonSchema[NodeView] =
    genericJsonSchema[NodeView]
  implicit lazy val edgeViewSchema: JsonSchema[EdgeView] =
    genericJsonSchema[EdgeView]

}
