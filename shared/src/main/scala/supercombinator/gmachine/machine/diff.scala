package supercombinator.gmachine.machine

import io.circe.generic.JsonCodec
import supercombinator.gmachine.machine.Utils.{hashIdx, md5}

@JsonCodec
case class NodeView(
  id: Int,
  label: String,
  from: Option[Int],
  to: Option[Int],
)
@JsonCodec
case class EdgeView(
  from: Int,
  to: Int,
)

@JsonCodec
case class GraphDiff(
  nodes: List[NodeView],
  edges: List[EdgeView],
)

@JsonCodec
case class Diff(
  command: String,
  add: GraphDiff,
  remove: GraphDiff,
  stack: List[NodeView],
  hasError: Boolean = false,
) {
  def withHash(hash: String): DiffWithHash =
    DiffWithHash(command, add, remove, stack, hash)
}

@JsonCodec
case class DiffWithHash(
  command: String,
  add: GraphDiff,
  remove: GraphDiff,
  stack: List[NodeView],
  hash: String,
)

object DiffObj {
  def getDiffsWithHash(diffs: List[Diff]): List[DiffWithHash] =
    diffs.zipWithIndex.map { case (diff, idx) =>
      val hash = hashIdx(idx, diff.hasError, md5)
      diff.withHash(hash)
    }
}

@JsonCodec
case class DiffResponse(
  diff: List[DiffWithHash],
  err: Option[String],
  result: Option[String],
)
