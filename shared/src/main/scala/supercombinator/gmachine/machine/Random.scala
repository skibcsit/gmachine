package supercombinator.gmachine.machine

case class RandomOnce(
  wasTargetEmitted: Boolean,
  randomRatio: Double, // true probability
  rnd: scala.util.Random,
) {
  def next(): (RandomOnce, Boolean) = {
    val result = if (wasTargetEmitted) {
      false
    } else {
      (1 - rnd.nextDouble()) < randomRatio
    }

    (RandomOnce(result || wasTargetEmitted, randomRatio, rnd), result)
  }
}
