package supercombinator.gmachine.machine
import java.security.MessageDigest

object Utils {
  def md5(s: String): String =
    MessageDigest
      .getInstance("MD5").digest(s.getBytes()).map(0xff & _).map {
        "%02x".format(_)
      }.foldLeft("")(_ + _)

  def hashIdx(idx: Int, hasError: Boolean, hashFn: String => String): String =
//    val str = if (hasError) {
//      s"${idx}_@#&_1"
//    } else {
//      s"${idx}_@#&_0"
//    }
    hashFn(s"${idx}_@#&_${hasError.toString}")
}
