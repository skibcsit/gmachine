val Http4sVersion = "0.21.4"
val LogbackVersion = "1.2.3"
val endpointsVersion = "1.0.0"
val tapirVersion = "0.16.7"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

lazy val app =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.skibcsit",
      scalaVersion := "2.13.4",
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "io.circe" %%% "circe-generic" % "0.13.0",
        "io.circe" %%% "circe-literal" % "0.13.0",
        "io.circe" %%% "circe-generic-extras" % "0.13.0",
        "io.circe" %%% "circe-parser" % "0.13.0",
        "com.outr" %%% "scribe" % "2.7.10",
        "org.typelevel" %%% "mouse" % "0.24",
        "org.typelevel" %%% "cats-core" % "2.1.1",
        "org.typelevel" %%% "cats-effect" % "2.1.3",
        "org.specs2" %%% "specs2-core" % "4.8.1" % Test,
        "org.endpoints4s" %%% "algebra" % endpointsVersion,
        "org.endpoints4s" %%% "json-schema-circe" % endpointsVersion,
        "org.endpoints4s" %%% "json-schema-generic" % endpointsVersion,
        "org.endpoints4s" %%% "algebra-json-schema" % endpointsVersion,
        "com.lihaoyi" %%% "fastparse" % "2.2.2",
      ),
      scalacOptions ++= Seq(
        "-Xlint:-byname-implicit,_",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-language:higherKinds",
        "-Ymacro-annotations",
        "-Ywarn-macros:after",
        "-language:reflectiveCalls",
      ),
      scalafmtOnCompile := true,
    )

lazy val appJS = app.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      "jitpack" at "https://jitpack.io",
    ),
    libraryDependencies ++= Seq(
      "com.github.OutWatch.outwatch" %%% "outwatch" % "584f3f2c32",
      "org.endpoints4s" %%% "xhr-client" % endpointsVersion,
      "io.monix" %%% "monix" % "3.1.0",
      "com.beachape" %%% "enumeratum" % "1.5.15",
      "com.chuusai" %%% "shapeless" % "2.3.3",
      "com.lihaoyi" %%% "utest" % "0.7.7" % Test
    ),
    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
      "dagre" -> "0.8.2",
      "sigma" -> "1.2.1",
      "md5" -> "2.3.0"
    ),
    jsDependencies ++= Seq(
      "org.webjars.npm" % "sigma" % "1.2.1" / "sigma.require.js",
      "org.webjars.npm" % "dagre" % "0.8.2" / "dagre.js",
      "org.webjars.npm" % "md5" % "2.3.0" / "md5.min.js",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("supercombinator.js.gmachine.components.GMachine"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm
  )

lazy val appJVM = app.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(appJS, Compile)).value,
    mappings.in(Universal) ++= webpack.in(Compile, fullOptJS).in(appJS, Compile).value.map { f =>
      f.data -> s"assets/${f.data.getName}"
    },
    mappings.in(Universal) ++= Seq(
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "css" / "bootstrap.min.css" ->
        "assets/bootstrap/dist/css/bootstrap.min.css",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "jquery" / "dist" / "jquery.slim.min.js" ->
        "assets/jquery/dist/jquery.slim.min.js",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "popper.js" / "dist" / "umd/popper.min.js" ->
        "assets/popper.js/dist/umd/popper.min.js",
      (target in(appJS, Compile)).value / ("scala-" + scalaBinaryVersion.value) / "scalajs-bundler" / "main" / "node_modules" / "bootstrap" / "dist" / "js/bootstrap.min.js" ->
        "assets/bootstrap/dist/js/bootstrap.min.js",
    ),
    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    mainClass in reStart := Some("supercombinator.jvm.Main"),
    libraryDependencies ++= Seq(
      "org.endpoints4s" %% "http4s-server" % endpointsVersion,
      "org.webjars" % "swagger-ui" % "3.20.9",
      "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-http4s" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
      "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion,
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "com.github.pureconfig" %% "pureconfig" % "0.12.1",
      "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2",
      "org.scalatest" %% "scalatest-flatspec" % "3.2.3" % Test,
    ),
  )

disablePlugins(RevolverPlugin)

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}
requireJsDomEnv in Test := true

scalaJSUseMainModuleInitializer := true
testFrameworks += new TestFramework("utest.runner.Framework")
herokuAppName in Compile := "js/src/main/scala/supercombinator/js/gmachine"

target in Compile := (target in(appJVM, Compile)).value
