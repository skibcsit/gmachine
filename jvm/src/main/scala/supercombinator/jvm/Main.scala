package supercombinator.jvm

import cats.effect.{Blocker, ExitCode, IO, IOApp, Resource}
import cats.syntax.semigroupk._
import endpoints4s.openapi.model.OpenApi
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import org.http4s.syntax.kleisli._
import pureconfig.ConfigSource
import scribe.{Level, Logger}
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import supercombinator.jvm.config.{AppConfig, SyncConfig}
import supercombinator.jvm.http.GMachineApiDocs.api
import supercombinator.jvm.http.{
  GCodeEndpoints,
  LambdaToGMachineEndpoints,
  StaticEndpoints,
}

import scala.concurrent.ExecutionContext

object Main extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      appConfig <- SyncConfig.read[IO, AppConfig](ConfigSource.default)
      exitCode  <- app(appConfig).use(_ => IO.never).as(ExitCode.Success)
    } yield exitCode

  private def app(appConfig: AppConfig): Resource[IO, Server[IO]] =
    for {
      blocker <- Blocker[IO]
      docRoutes = new SwaggerHttp4s(
        OpenApi.stringEncoder.encode(api),
        "docs",
        "docs.json",
      ).routes[IO]
      httpApp = (
        new LambdaToGMachineEndpoints[IO].lambdaToGMachineCompileRoutes <+>
          new GCodeEndpoints[IO].gCodeCompileRoutes <+>
          new StaticEndpoints[IO](appConfig.assets, blocker)
            .endpoints() <+> docRoutes
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(appConfig.http.port, appConfig.http.host)
        .withHttpApp(CORS(httpApp))
        .resource
    } yield server
}
