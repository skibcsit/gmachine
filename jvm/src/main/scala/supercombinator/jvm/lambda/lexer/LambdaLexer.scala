package supercombinator.jvm.lambda.lexer

import scala.util.parsing.combinator.RegexParsers
import scala.util.parsing.input.Positional

object LambdaLexer extends RegexParsers {
  def varName: Parser[VarName] = positioned {
    "[a-zA-z][a-zA-Z0-9_]*".r ^^ { str =>
      VarName(str)
    }
  }
  def intValue: Parser[IntValue] = positioned {
    """[-+]?[1-9]\d*|0""".r ^^ { str =>
      IntValue(str.toInt)
    }
  }
  def boolValue: Parser[BoolValue] = positioned {
    "TRUE|FALSE".r ^^ { str =>
      BoolValue(str.toBoolean)
    }
  }
  def lambdaStart: LambdaLexer.Parser[LambdaStart] = positioned {
    "\\" ^^ (_ => LambdaStart())
  }
  def leftBracket: LambdaLexer.Parser[LeftBracket] = positioned {
    "(" ^^ (_ => LeftBracket())
  }
  def rightBracket: LambdaLexer.Parser[RightBracket] = positioned {
    ")" ^^ (_ => RightBracket())
  }
  def comma: LambdaLexer.Parser[Comma] = positioned("," ^^ (_ => Comma()))
  def arrow: LambdaLexer.Parser[Arrow] = positioned("->" ^^ (_ => Arrow()))
  def let: LambdaLexer.Parser[LetToken] = positioned {
    "let" ^^ (_ => LetToken())
  }
  def letRec: LambdaLexer.Parser[LetRecToken] = positioned {
    "letrec" ^^ (_ => LetRecToken())
  }
  def assign: LambdaLexer.Parser[Assign] = positioned("=" ^^ (_ => Assign()))
  def in: LambdaLexer.Parser[In] = positioned("in" ^^ (_ => In()))
  def sum: LambdaLexer.Parser[SUM] = positioned("+" ^^ (_ => SUM()))
  def sub: LambdaLexer.Parser[SUB] = positioned("-" ^^ (_ => SUB()))
  def mul: LambdaLexer.Parser[MUL] = positioned("*" ^^ (_ => MUL()))
  def div: LambdaLexer.Parser[DIV] = positioned("/" ^^ (_ => DIV()))
  def gte: LambdaLexer.Parser[GE] = positioned(">=" ^^ (_ => GE()))
  def lte: LambdaLexer.Parser[LE] = positioned("<=" ^^ (_ => LE()))
  def lt: LambdaLexer.Parser[LT] = positioned("<" ^^ (_ => LT()))
  def gt: LambdaLexer.Parser[GT] = positioned(">" ^^ (_ => GT()))
  def _eq: LambdaLexer.Parser[EQ] = positioned("==" ^^ (_ => EQ()))
  def _ne: LambdaLexer.Parser[NE] = positioned("!=" ^^ (_ => NE()))
  def _if: LambdaLexer.Parser[IF] = positioned("IF" ^^ (_ => IF()))
  def ops: LambdaLexer.Parser[LambdaToken] = sum | sub | mul | div
  def cmps: LambdaLexer.Parser[LambdaToken] = gte | lte | lt | gt | _ne | _eq
  def brackets: LambdaLexer.Parser[LambdaToken] = leftBracket | rightBracket

  def tokens: Parser[List[LambdaToken]] =
    phrase(
      rep1(
        comma | arrow | ops | cmps | brackets | lambdaStart | intValue | boolValue | _if | letRec | let | assign | in | varName | failure(
          "empty input",
        ),
      ),
    ) ^^ (tokens => tokens)
  def Parse(code: String): Either[String, List[LambdaToken]] =
    parse(tokens, code) match {
      case NoSuccess(msg, next) =>
        Left(s"$msg, at ${next.pos.line}:${next.pos.column}")
      case Success(result, next) => Right(result)
    }
}
