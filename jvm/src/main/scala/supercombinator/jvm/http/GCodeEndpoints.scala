package supercombinator.jvm.http

import cats.effect.Async
import cats.implicits._
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromSchemas}
import org.http4s.HttpRoutes
import supercombinator.GMachineApiEndpoints
import supercombinator.gmachine.machine.Machine
import supercombinator.gmachine.machine.parser.GCodeParser
import supercombinator.models.Error

final class GCodeEndpoints[F[_]](implicit
  F: Async[F],
) extends Endpoints[F]
    with JsonEntitiesFromSchemas
    with GMachineApiEndpoints {

  val gCodeCompileRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    gCodeCompile.implementedBy(input =>
      GCodeParser.parseGCode(input.code) match {
        case Left(err) => Error(err).asLeft
        case Right(s) =>
          Machine
            .run(s, input.onlyResult, withErrors = input.withErrors).asRight
      },
    )
  }

}
