package supercombinator.jvm.http

import endpoints4s.openapi
import endpoints4s.openapi.model.{Info, OpenApi}
import supercombinator.GMachineApiEndpoints

object GMachineApiDocs
    extends GMachineApiEndpoints
    with openapi.Endpoints
    with openapi.JsonEntitiesFromSchemas {

  val api: OpenApi =
    openApi(
      Info(title = "The G-Machine", version = "1.0.0"),
    )(gCodeCompile, lambdaToGMachineCompile)
}
