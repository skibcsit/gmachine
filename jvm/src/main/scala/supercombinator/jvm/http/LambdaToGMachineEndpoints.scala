package supercombinator.jvm.http

import cats.effect.Async
import cats.implicits._
import endpoints4s.http4s.server.{Endpoints, JsonEntitiesFromSchemas}
import org.http4s.HttpRoutes
import supercombinator.GMachineApiEndpoints
import supercombinator.jvm.lambda.compiler.Compiler
import supercombinator.jvm.lambda.lexer.LambdaLexer
import supercombinator.jvm.lambda.parser.LambdaParser
import supercombinator.jvm.lambda.supercombinator.SuperCombinator
import supercombinator.models.{Error, Output}

final class LambdaToGMachineEndpoints[F[_]](implicit
  F: Async[F],
) extends Endpoints[F]
    with JsonEntitiesFromSchemas
    with GMachineApiEndpoints {

  val lambdaToGMachineCompileRoutes: HttpRoutes[F] =
    HttpRoutes.of[F] {
      lambdaToGMachineCompile.implementedByEffect(input =>
        F.delay(
          LambdaLexer
            .Parse(input.code)
            .flatMap(tokens =>
              LambdaParser
                .Parse(tokens)
                .flatMap { value =>
                  val sp = SuperCombinator.LambdaLifting(value)
                  Right(
                    Output(Compiler.Compile(sp).map(_.toString)),
                  )
                },
            ) match {
            case Left(a)  => Error(a).asLeft
            case Right(b) => b.asRight
          },
        ),
      )
    }

}
